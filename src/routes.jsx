import React from 'react';
import { Switch, Route } from 'react-router-dom';

import screens from './screens';

export default (
  <Switch>
    <Route path="/" exact component={screens.Home} />
    <Route path="/p" component={screens.MainLayout}/>
  </Switch>
);