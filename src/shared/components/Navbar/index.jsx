import React from 'react';
import { Link } from 'react-router-dom';

import style from './Navbar.module.css';

const Navbar = () => (
  <header>
    <Link className={style.link} to="/">Home</Link>
    <Link className={style.link} to="/p/movies">Movies</Link>
  </header>
);

export default Navbar;