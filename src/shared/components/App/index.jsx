import React from 'react';
import T from 'prop-types';
import classnames from 'classnames/bind';

import Navbar from '../Navbar';
import styles from './App.module.css';

const cx = classnames.bind(styles);

const appCx = ({ red }) => cx(
  styles.app,
  { redColor: red }
);

const App = ({ children }) => (
  <div className={appCx({ red: true })}>
    <Navbar />
    {children}
  </div>
);

// App.propTypes = {
//   children: T.React.Component
// };

export default App;