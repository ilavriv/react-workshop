import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import movies from '../../movies/reducers';

import { moviesSaga } from '../../movies/sagas'

const appReducer = combineReducers({
  movies
});

function* rootSaga() {
  return yield all([
    moviesSaga()
  ]);
}

const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
  const store = createStore(
    appReducer,
    applyMiddleware(sagaMiddleware)
  );

  sagaMiddleware.run(rootSaga);

  return store;
};

export default configureStore;