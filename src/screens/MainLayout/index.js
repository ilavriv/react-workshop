import React from 'react';

import { Switch, Route } from 'react-router-dom'; 

import Movies from './Movies';

const MainLayout = () => (
  <>
    <h1>Main</h1>

    <Switch>
      <Route path="/p/movies" component={Movies} />
    </Switch>
  </>
);

export default MainLayout;