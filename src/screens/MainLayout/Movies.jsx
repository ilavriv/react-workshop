import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import MoviesAPI from '../../movies';

class Movies extends Component {

  componentDidMount() {
    const { moviesActions } = this.props;

    moviesActions.fetch();
  }

  render() {
    const {
      movies,
      active,
      moviesActions
    } = this.props;

    const categoriesList = Object.values(MoviesAPI.constants.CATEGORIES);

    return (
      <div>
        <MoviesAPI.components.CategoriesList
          active={active}
          setActive={moviesActions.setActiveCategory}
          categories={categoriesList}
        />
        <MoviesAPI.components.MoviesList
          movies={movies}
        />
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  movies: MoviesAPI.selectors.getList,
  active: MoviesAPI.selectors.getActive
});

const mapDispatchToProps = dispatch => ({
  moviesActions: bindActionCreators(MoviesAPI.actions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Movies);