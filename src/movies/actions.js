import {
  SET_ACTIVE_CATEGORY,
  FETCH_MOVIES_REQUEST
} from './action-types';


export const setActiveCategory = category => ({
  type: SET_ACTIVE_CATEGORY,
  category
});

export const fetch = () => ({
  type: FETCH_MOVIES_REQUEST
});