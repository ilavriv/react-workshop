import { CATEGORIES } from './constants';


export const getList = ({
  movies
}) => {
  const { list, activeCategory } = movies;

  if (activeCategory === CATEGORIES.all) {
    return list;
  }

  return list.filter(({ category }) => category === activeCategory);
};

export const getActive = ({ movies }) => movies.activeCategory;