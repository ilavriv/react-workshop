import { takeEvery, call, put } from 'redux-saga/effects';
import moviesApi from './api';

import {
  FETCH_MOVIES_REQUEST,
  FETCH_MOVIES_SUCCESS,
  FETCH_MOVIES_FAILED
} from './action-types';


function* fetchMovies() {
  try {
    const movies = yield call(moviesApi.fetch);

    yield put({ type: FETCH_MOVIES_SUCCESS, movies });
  } catch(err) {
    yield put( { type: FETCH_MOVIES_FAILED, movies: [] });
  }
};

export function* moviesSaga() {
  yield takeEvery(FETCH_MOVIES_REQUEST, fetchMovies);
};

