import { apiFetch } from '../utils/fetch';

export default {
  fetch() {
    return apiFetch('/movies');
  }
}