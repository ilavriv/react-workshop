import React from 'react';

const MoviesList = ({ movies }) => <div>
  {movies.map(({ name, director, poster }, i) => <div key={i}>
    <h4>{name}</h4>
    <div>
      <img src={poster} alt=""/>
    </div>
    <div>by {director}</div>
  </div>)}
</div>;

export default MoviesList;