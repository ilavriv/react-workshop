import MoviesList from './MoviesList';
import CategoriesList from './CategoriesList';

export default {
  MoviesList,
  CategoriesList
};