import React from 'react';
import classnames from 'classnames/bind';

import style from './CategoriesList.module.css';

const cx = classnames.bind(style);

const CategoriesList = ({
  active,
  setActive,
  categories
}) => <div>
  {categories.map((category, i) => (
  <span
    key={i}
    onClick={() => setActive(category)}
    className={cx('category', {
      active: active === category
    })}
  >
    {category.toUpperCase()}</span>
  )
)}
</div>;


export default CategoriesList;