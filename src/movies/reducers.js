import { combineReducers } from 'redux';
import { CATEGORIES } from './constants';
import * as t from './action-types';


const list = (state = [], action) => {
  console.log('a', action);

  switch(action.type) {
    case t.FETCH_MOVIES_SUCCESS:
      return action.movies;
    default:
      return state;
  }
};

const activeCategory = (
  state = CATEGORIES.all,
  action
) => action.type === t.SET_ACTIVE_CATEGORY ? action.category : state;

export default combineReducers({
  list,
  activeCategory
});

