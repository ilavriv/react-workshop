export const CATEGORIES = Object.freeze({
  all: 'all',
  movie: 'movie',
  tvShow: 'tvShow'
});