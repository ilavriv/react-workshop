const API_URL = 'http://localhost:8080';

export const apiFetch = (endpoint) => {
  return window.fetch(`${API_URL}${endpoint}`)
    .then(res => res.json());
}