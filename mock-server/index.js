const api = require('express')();


api.use(require('cors')());

api.get('/:name', (req, res) => {
  const { name } = req.params;

  const dataset = require(`./mocks/${name}.json`);

  res.json(dataset).end();
});


api.listen(8080, () => console.log('App run'));